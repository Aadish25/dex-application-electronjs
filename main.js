console.log('main process working')

const electron=require("electron")

const app=electron.app;
const BrowserWindow=electron.BrowserWindow;
const path=require("path");
const url=require("url");

let mainWindow;

function createWindow(){
    mainWindow=new BrowserWindow({
       height:450,
       width:800,
       webPreferences:{
        nodeIntegration:true
       } 
    });
    mainWindow.loadURL(url.format({
        pathname:path.join(__dirname,'index.html'),
        protocol:'file',
        slashes:true
    }));

    mainWindow.on('closed',()=>{
        mainWindow=null;
    })

}

app.on('ready',createWindow);
